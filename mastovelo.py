#! /usr/bin/env python3

# Source
# https://github.com/Chealion/yycbike/blob/main/twitterBot.py

# Blagnac
# https://www.eco-visio.net/api/aladdin/1.0.0/pbl/publicwebpageplus/data/300021017?idOrganisme=4586&idPdc=300021017&fin=08%2F01%2F2022&debut=01%2F01%2F2022&interval=4&flowIds=353291465%3B353291466%3B353291467%3B353291468

# Matabiau
# https://www.eco-visio.net/api/aladdin/1.0.0/pbl/publicwebpageplus/data/300018434?idOrganisme=4586&idPdc=300018434&fin=08%2F01%2F2022&debut=01%2F01%2F2022&interval=4&flowIds=353274003%3B353274004

# St Martin
# https://www.eco-visio.net/api/aladdin/1.0.0/pbl/publicwebpageplus/data/300021015?idOrganisme=4586&idPdc=300021015&fin=08%2F01%2F2022&debut=01%2F01%2F2022&interval=4&flowIds=353291457%3B353291458

import json
import time
import sys
import os
import logging
import locale
from datetime import date, timedelta
from urllib.request import urlopen
from mastodon import Mastodon, MastodonError


################################################################################
# Mastodon info
mast_instance = "botsin.space"
mast_account = "account"
mast_password = "password"

# Long list of counters to pull data from. There are two styles - those that
# only count bikes and only have an ID And those that count multiple items and
# as such have a component section needed to grab the correct numbers These IDs
# can be found in the README or otherwise at the URL:
BLAGNAC = {
    'installation':"Blagnac",
    'value': 0,
    'id': '300021017',
    'components': '353291465;353291466;353291467;353291468'
}
STMARTIN = {
    'installation': "Site Airbus St Martin",
    'value': 0,
    'id': '300021015',
    'components': '353291457;353291458'
}
MATABIAU = {
    'installation': "Matabiau",
    'value': 0,
    'id': '300018434',
    'components': '353274003;353274004'
}

# Count and locations
yesterday = { "BLAGNAC": 0, "STMARTIN": 0, "MATABIAU": 0}

# Emojis
emo_cyclist = "\U0001F6B4"
emo_bike = "\U0001F6B2"
emo_stat = "\U0001F4CA"

# Set locales
locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')

################################################################################

def login(instance, account, password):
    """
    Login to Mastodon. Fonction copiée de 
    https://gitlab.com/jeancf/twoot/-/blob/master/twoot.py
    """
    # Create Mastodon application if it does not exist yet
    if not os.path.isfile(instance + '.secret'):
        try:
            Mastodon.create_app(
                'rZ1fo7Hk60vOANzlEA4gVMDNldm4DdE4Xbpo1LcCD10',
                api_base_url = 'https://' + instance,
                to_file = instance + '.secret'
            )

        except MastodonError as me:
            logging.fatal('failed to create app on ' + instance)
            logging.fatal(me)
            sys.exit(-1)

    # Log in to Mastodon instance
    try:
        mastodon = Mastodon(
            client_id = instance + '.secret',
            api_base_url = 'https://' + instance
        )
        
        mastodon.log_in(
            username = account,
            password = password,
            to_file = account + ".secret"
        )
        logging.info('Logging in to ' + instance)

    except MastodonError as me:
        logging.fatal('ERROR: Login to ' + instance + ' Failed\n')
        logging.fatal(me)
        sys.exit(-1)

    return mastodon


def grabCounts(installation, yesterday, today):
    """
    Get the count from eco-visio API. 
    https://github.com/Chealion/yycbike/blob/main/twitterBot.py
    """
    # City of Toulouse domain id
    domain = '4586'
    baseURL = "www.eco-visio.net/api/aladdin/1.0.0/pbl/publicwebpageplus/data/"
    countURL = "https://" + baseURL + installation['id'] + "?idOrganisme=" + \
            domain + "&idPdc=" + installation['id'] + "&fin=" + today + \
            "&debut=" + yesterday + "&interval=4&flowIds=" + \
            installation['components'] 
    amount = 0
    try:
        response = urlopen(countURL)
        json_data = response.read()
        loaded_data = json.loads(json_data)

        # Handle Case where multiple values are available for the daily total
        # (eg. Lindsay Park counter)
        if len(loaded_data) > 1:
            logging.info(f"More than one entry encountered on {id}")
            for entry in loaded_data:
                if entry[1] is not None:
                    amount += entry[1]
        else:
            amount = loaded_data[0][1]

    except:
        amount = 0

        if amount is None or amount == 0:
            amount = 0
            logging.fatal(f"Error loading - amount is None (counter down?) for {id} - {countURL}")
        else:
            logging.fatal(f"Error loading {id} - {countURL}")

    return int(amount)



def last_day_of_month(any_day):
    """ 
    Return the last day of the month (28, 29, 30, 31)...
    """
    return any_day - timedelta(days=any_day.day)


def grabMonthlyStats(installation, month, year):
    """
    Get the count from eco-visio API. 
    https://github.com/Chealion/yycbike/blob/main/twitterBot.py
    """
    # City of Toulouse domain id
    domain = '4586'
    baseURL = "www.eco-visio.net/api/aladdin/1.0.0/pbl/publicwebpageplus/data/"
    lastDay = last_day_of_month(date(int(year), int(month), 1)).strftime('%d')


    monthlyStat = "https://" + baseURL + installation['id'] + "?idOrganisme=" + \
            domain + "&idPdc=" + installation['id'] + "&fin=" + lastDay + \
            "/" + str(month) + "/" + str(year) + "&debut=01/" + str(month) + \
            "/" + str(year) + "&interval=4&flowIds=" + \
            installation['components'] 
    stats = []

    try:
        response = urlopen(monthlyStat)
        json_data = response.read()
        loaded_data = json.loads(json_data)

        for day in loaded_data:
            stats.append(int(day[1]))

    except:
        stat = []
        logging.fatal(f"Error loading montly stats {monthlyStat}")

    return stats


def writeTootProgression(progMonth, progYear, lastMonth, lastLastMonth, lastYear):
    """
    Compute progression since previous month, and since the same month from the
    previous year.
    """

    if progMonth>0:
        tootProgMonth = "\n    * +" + str(progMonth) + "% par rapport à " + \
                lastLastMonth.strftime('%B')
    elif progMonth == 0:
        tootProgMonth = ""
    else: 
        tootProgMonth = "\n    * " + str(progMonth) + "% par rapport à " \
                + lastLastMonth.strftime('%B')

    if progYear>0:
        tootProgYear = "\n    * +" + str(progYear) + "% par rapport à " \
                + lastMonth.strftime('%B') + " " + str(lastYear)
    elif progYear == 0:
        tootProgYear = ""
    else: 
        tootProgYear = "\n    * " + str(progYear) + "% par rapport à " \
                + lastMonth.strftime('%B') + " " + str(lastYear)

    return tootProgMonth + tootProgYear




# Timezones make time math hard. Also convert the time into three formats.
yesterday = date.today() - timedelta(1)
today = date.today()
fancyDate = yesterday.strftime('%a %b %d')

# Timezones make time math hard. Also convert the time into three formats.
yesterday = date.today() - timedelta(1)
today = date.today()
fancyDate = yesterday.strftime('%a %b %d')
yesterday = yesterday.strftime('%d/%m/%Y')
today = today.strftime('%d/%m/%Y')

for installation in [ BLAGNAC, STMARTIN, MATABIAU ]:
    logging.info("Attempting to grab " + installation['installation'])
    try:
        installation['value'] = grabCounts(installation, yesterday, today)
        logging.info(installation['installation'] + " : " + \
                str(installation['value']))
    except:
        logging.fatal("Failed to load installation info - skipping")
        logging.fatal("Unexpected error:", sys.exc_info()[0])


# Login mastodon
mastodon = login(mast_instance, mast_account, mast_password)

# Create and post the daily toot
tootContent = emo_stat + " Hier, les totems vélo toulousains ont compté le nombre de passages suivant : \n  - Blagnac : " + str(BLAGNAC['value']) + \
        "\n  - Matabiau : " + str(MATABIAU['value']) + \
        "\n  - St Martin du Touch : " + str(STMARTIN['value'])

toot = mastodon.status_post(tootContent, visibility='public')

# If first day of the month: toot the stats of last month month: show progress
# since the previous month (last last month, and from the same month the
# previous year)
if date.today().strftime('%d') == "01":
    # Last month number and year (month that ended yesterday)
    lastMonth = (date.today() - timedelta(1))
    lastMonthNb = lastMonth.strftime('%m')
    lastMonthYear = (date.today() - timedelta(1)).strftime('%Y')

    # Previous month number and year 
    lastLastMonth = date.today() - timedelta(1) - relativedelta(months=1)
    lastLastMonthYear = lastLastMonth.strftime('%Y')
    lastLastMonthNb = lastLastMonth.strftime('%m')

    prettyDate = (date.today()-timedelta(1)).strftime('%B')+" "+lastMonthYear
    tootStat = emo_stat + emo_bike + " C'est l'heure du bilan de "
    tootStat += prettyDate+" ! Le mois dernier :\n"
    for installation in [ BLAGNAC, STMARTIN, MATABIAU ]:
        logging.info("Attempting to grab " + installation['installation'])
        try:

            # Get stats from API
            lastMonthStat = grabMonthlyStats(installation, lastMonthNb, \
                    lastMonthYear) 
            lastMonthYearStat = grabMonthlyStats(installation, lastMonthNb, \
                    lastLastMonthYear)
            lastLastMonthStat = grabMonthlyStats(installation, \
                    lastLastMonthNb, int(lastMonthYear)-1)
            location = installation['installation'] 

            # Compute total
            totalLastMonth = sum(lastMonthStat)
            totalLastLastMonth = sum(lastLastMonthStat)
            totalLastMonthYear = sum(lastMonthYearStat)

            # Progression from previous month and same month the previous year
            if len(lastMonthStat) != 0:
                progMonth = 100 * (totalLastMonth - totalLastLastMonth) / totalLastLastMonth
            else:
                progMonth = 0
            if len(lastMonthYearStat) != 0:
                progYear = 100*(totalLastMonth - totalLastMonthYear) / totalLastMonthYear
            else:
                progYear = 0

            # Write toot
            tootProg = writeTootProgression(int(progMonth), int(progYear), \
                    lastMonth, lastLastMonth, int(lastMonthYear)-1)

            tootStat += "  - "+location+" : " + str(totalLastMonth) + \
                    " passages" + tootProg + "\n"
        except:
            logging.fatal("Failed to compute monthly stats, skipping")
            logging.fatal("Unexpected error:", sys.exc_info()[0])

    toot = mastodon.status_post(tootStat, visibility='public')



